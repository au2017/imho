export default class {
    constructor() {
        document.querySelectorAll('.delete-link').forEach(a => this.addClick(a));
    }

    success(resp) {
        if (!resp.ok) {
            throw new Error('Problem deleting item');
        }

        resp.json().then(data => location.href = data.redirect);
    }

    deleteGame(e) {
        e.preventDefault();

        if (!confirm('Are you sure you want to delete this item?')) {
            return false;
        }

        fetch(e.target.href, {
            credentials: 'same-origin',
            method: 'delete'
        }).then(this.success).catch(err => console.log(err));

        return false;
    }

    addClick(a) {
        a.addEventListener('click', e => this.deleteGame(e));
    }
}
