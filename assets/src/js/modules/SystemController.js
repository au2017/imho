export default class {

    deleteSystem(id) {
        if (!confirm('Are you sure?')) {
            return false;
        }

        const success = resp => {
            if (!resp.ok) {
                throw new Error('Problem deleting system with id ' + id);
            }
            this.removeSystem(id);
        };

        fetch(`/api/system/${id}`, {
            method: 'delete',
        }).then(success).catch(err => console.log(err));
    }

    removeSystem(id) {
        const systems = document.getElementById('systems');
        let system;

        for (let i = 0; i < systems.children.length; i++) {
            system = systems.children.item(i);

            if (system.dataset.id == id) {
                return systems.removeChild(system);
            }
        }
    }

    editSystem(id) {
        const name = prompt('System name:');
        const success = resp => {
            if (!resp.ok) {
                throw new Error('Problem editing system');
            }
            resp.json().then(data => {
                // update system name
                var systems = Array.from(document.querySelectorAll('li'));
                var system = systems.find(li => li.dataset.id == id);
                if (system) {
                    system.childNodes.forEach(el => {
                        if (el.className === 'title') {
                            el.innerHTML = name;
                        }
                    })
                }
            });
        };
        const headers = new Headers({'Content-Type': 'application/json'});

        if (!id || !name) {
            return false;
        }

        fetch('/api/system', {
            headers: headers,
            method: 'put',
            body: JSON.stringify({id: id, name: name}),
        }).then(success).catch(err => console.log(err));
    }

    saveSystem() {
        const name = prompt('System name:');
        const success = resp => {
            if (!resp.ok) {
                throw new Error('Problem adding system');
            }
            resp.json().then(data => this.addSystem(data));
        };
        const headers = new Headers({
            'Content-Type': 'application/json',
        });

        // remove query param from URL if there is one
        history.replaceState({}, 'IMHO - Video Game Blog', '/admin/systems');

        if (!name) {
            return false;
        }

        fetch('/api/system', {
            headers: headers,
            method: 'post',
            body: JSON.stringify({ name: name }),
        }).then(success).catch(err => console.log(err));
    }

    addSystem(data) {
        const { systemName, id } = data;
        const systems = document.getElementById('systems');
        const item = document.createElement('li');
        item.dataset.id = id;
        item.innerHTML = `
            <span class="title">${systemName}</span>
            <button class="edit-btn">Edit</button>
            <button class="delete-btn">Delete</button>`;
        [].slice.call(systems.children)
            .concat(item)
            .sort(this.sortSystems)
            .forEach(i => systems.appendChild(i));
    }

    sortSystems(a, b) {
        let spanA, spanB;

        for (let i = 0; i < a.childNodes.length; i++) {
            if (a.childNodes[i].className === 'title') {
                spanA = a.childNodes[i];
                break;
            }
        }

        for (let j = 0; j < b.childNodes.length; j++) {
            if (b.childNodes[j].className === 'title') {
                spanB = b.childNodes[j];
                break;
            }
        }
        return spanA.innerText.toLowerCase() > spanB.innerText.toLowerCase();
    }
}
