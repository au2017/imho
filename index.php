<?php
use \Slim\Middleware\Session;
use \Imho\Controllers\HomeController as HomeController;
use \Imho\Controllers\AccountController as AccountController;
use \Imho\Controllers\AdminController as AdminController;
use \Imho\Controllers\Api\SystemController as SystemController;

require './vendor/autoload.php';

// create app
$app = new \Imho\MyApp();

// add middleware
$app->add(new Session([
    'autorefresh' => true,
    'lifetime' => '1 hour',
]));

// create routes
$app->get('/', [HomeController::class, 'home']);
$app->get('/new-user', [AccountController::class, 'getNewUserPage']);
$app->post('/new-user', [AccountController::class, 'postNewUserPage']);
$app->get('/login', [AccountController::class, 'getLoginPage']);
$app->post('/login', [AccountController::class, 'postLoginPage']);
$app->get('/logout', [AccountController::class, 'logout']);

// admin routes
$app->get('/admin', [AdminController::class, 'getIndex']);
$app->get('/admin/systems', [AdminController::class, 'getSystems']);
$app->get('/admin/games', [AdminController::class, 'getGames']);
$app->get('/admin/game-form[/{id}]', [AdminController::class, 'gameForm']);
$app->post('/admin/game-form', [AdminController::class, 'saveGame']);
$app->delete('/admin/games/{id}', [AdminController::class, 'deleteGame']);
$app->get('/admin/blogs', [AdminController::class, 'getBlogs']);
$app->get('/admin/blog-form[/{id}]', [AdminController::class, 'blogForm']);
$app->post('/admin/blog-form', [AdminController::class, 'saveBlog']);
$app->delete('/admin/blogs/{id}', [AdminController::class, 'deleteBlog']);

// api routes
$app->post('/api/system', [SystemController::class, 'post']);
$app->put('/api/system', [SystemController::class, 'put']);
$app->delete('/api/system/{id}', [SystemController::class, 'delete']);

// run app
$app->run();
