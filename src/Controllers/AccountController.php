<?php
namespace Imho\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Views\Twig;
use \Imho\Services\Interfaces\IAccountService;

final class AccountController
{
    private $view;
    private $accountService;

    /**
     * @Inject("session")
     */
    private $session;

    public function __construct(Twig $view, IAccountService $acctService)
    {
        $this->view = $view;
        $this->accountService = $acctService;
    }

    public function getNewUserPage(Request $request, Response $response)
    {
        return $this->view->render($response, 'new-user.html.twig');
    }

    public function postNewUserPage(Request $request, Response $response)
    {
        $success = $this->accountService->createNewUser(
            $request->getParam('username'),
            $request->getParam('pass'),
            $request->getParam('confirm')
        );

        if ($success) {
            return $response->withRedirect('/login');
        }

        return $this->view->render($response, 'new-user.html.twig', [
            'msg' => 'Problem creating new user',
        ]);
    }

    public function getLoginPage(Request $request, Response $response)
    {
        return $this->view->render($response, 'login.html.twig');
    }

    public function postLoginPage(Request $request, Response $response)
    {
        try {
            $username = $request->getParam('username');
            $pass = $request->getParam('pass');

            if (empty($username) || empty($pass)) {
                throw new \Exception('All fields required');
            }

            $user = $this->accountService->getUser($username);

            if ($user->validateLogin($pass)) {
                $this->session->loggedIn = true;
                $this->session->user = $user;

                return $response->withRedirect('/');
            }

            throw new \Exception('Problem logging in');
        } catch (\Throwable $e) {
            return $this->view->render($response, 'login.html.twig', [
                'msg' => $e->getMessage(), // will be user-friendly
            ]);
        }
    }

    public function logout(Request $request, Response $response) {
        $this->session::destroy();

        return $response->withRedirect('/');
    }
}
