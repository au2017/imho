<?php
namespace Imho\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Views\Twig;
use \Imho\Services\Interfaces\ISystemService;
use \Imho\Services\Interfaces\IGameService;
use \Imho\Services\Interfaces\IBlogService;
use \Imho\Models\Game;
use \Imho\Models\Blog;

final class AdminController
{
    private $view;

    /**
     * @Inject("session")
     */
    private $session;

    private $systemService;
    private $gameService;
    private $blogService;

    public function __construct(Twig $view, ISystemService $sysService, IGameService $gameService, IBlogService $blogService)
    {
        $this->view = $view;
        $this->systemService = $sysService;
        $this->gameService = $gameService;
        $this->blogService = $blogService;
    }

    public function getIndex(Request $request, Response $response)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        return $this->view->render($response, 'admin/index.html.twig', [
            'title' => 'Admin - Index',
            'user' => $this->session->user,
        ]);
    }

    public function getSystems(Request $request, Response $response)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        return $this->view->render($response, 'admin/systems.html.twig', [
            'title' => 'Admin - Systems',
            'systems' => $this->systemService->getSystems(),
            'user' => $this->session->user,
            'create' => $request->getParam('create'),
        ]);
    }

    public function getGames(Request $request, Response $response)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        return $this->view->render($response, 'admin/games.html.twig', [
            'title' => 'Admin - Games',
            'games' => $this->gameService->getGames(),
            'user' => $this->session->user,
        ]);
    }

    public function gameForm(Request $request, Response $response, $id = null)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        return $this->view->render($response, 'admin/game-form.html.twig', [
            'title' => 'Admin - New Game',
            'systems' => $this->systemService->getSystems(),
            'user' => $this->session->user,
            'game' => $id ? $this->gameService->getGame($id) : new Game(),
        ]);
    }

    public function saveGame(Request $request, Response $response)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        $id = $request->getParam('id');
        $title = $request->getParam('title');
        $system = $request->getParam('system');
        $year = $request->getParam('year');
        $completed = $request->getParam('completed') ?? false;

        if (!isset($id) || empty($id)) {
            $id = $this->gameService->addGame(
                $title,
                $system,
                $year,
                $completed
            );
        } else {
            $success = $this->gameService->editGame(
                $id,
                $title,
                $system,
                $year,
                $completed
            );
        }

        if ($id || $success) {
            return $response->withRedirect('/admin/games');
        }

        return $this->view->render($response, 'admin/game-form.html.twig', [
            'title' => 'Admin - New Game',
            'systems' => $this->systemService->getSystems(),
            'user' => $this->session->user,
            'msg' => 'Problem saving game',
        ]);
    }

    public function deleteGame(Request $request, Response $response, $id)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        $success = $this->gameService->deleteGame($id);

        return $response->withJson([
            'status' => $success ? 'success' : 'error',
            'redirect' => '/admin/games',
        ]);
    }

    public function saveBlog(Request $request, Response $response)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        $success = false;
        $id = $request->getParam('id');
        $title = $request->getParam('title');
        $gameId = $request->getParam('game');
        $body = $request->getParam('body');
        $userId = $this->session->user->id;

        if (!isset($id) || empty($id)) {
            $id = $this->blogService->addBlog($title, $body, $gameId, $userId);
        } else {
            $success = $this->blogService->editBlog($id, $title, $body, $gameId,
                $userId);
        }

        if ($id || $success) {
            return $response->withRedirect('/admin/blogs');
        }

        return $this->view->render($response, 'admin/blog-form.html.twig', [
            'title' => 'Admin - New Blog',
            'games' => $this->gameService->getGames(),
            'user' => $this->session->user,
            'msg' => 'Problem saving blog',
        ]);
    }

    public function getBlogs(Request $request, Response $response)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        return $this->view->render($response, 'admin/blogs.html.twig', [
            'title' => 'Admin - Blogs',
            'blogs' => $this->blogService->getBlogs(),
            'user' => $this->session->user,
        ]);
    }

    public function deleteBlog(Request $request, Response $response, $id)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        $success = $this->blogService->deleteBlog($id);

        return $response->withJson([
            'status' => $success ? 'success' : 'error',
            'redirect' => '/admin/blogs',
        ]);
    }

    public function blogForm(Request $request, Response $response, $id = null)
    {
        if (!$this->isAuthorized()) {
            return $response->withRedirect('/');
        }

        return $this->view->render($response, 'admin/blog-form.html.twig', [
            'title' => 'Admin - New Blog',
            'games' => $this->gameService->getGames(),
            'user' => $this->session->user,
            'blog' => $id ? $this->blogService->getBlog($id) : new Blog(),
        ]);
    }

    private function isAuthorized() : bool
    {
        return isset($this->session->user) && $this->session->user->isAdmin;
    }
}
