<?php
namespace Imho\Controllers\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Imho\Services\Interfaces\ISystemService;

final class SystemController
{
    private $systemService;

    public function __construct(ISystemService $sysService)
    {
        $this->systemService = $sysService;
    }

    public function delete(Request $request, Response $response, $id)
    {
        $success = $this->systemService->deleteSystem($id);

        if (!$success) {
            return $response->withJson(['status' => 'error'], 500);
        }

        return $response->withJson(['status' => 'success']);
    }

    public function post(Request $request, Response $response)
    {
        $name = $request->getParam('name');
        $id = $this->systemService->addSystem($name);

        if (!$id) {
            return $response->withJson(['status' => 'error'], 500);
        }

        return $response->withJson([
            'status' => 'success',
            'systemName' => $name,
            'id' => $id,
        ]);
    }

    public function put(Request $request, Response $response)
    {
        $id = $request->getParam('id');
        $name = $request->getParam('name');

        $success = $this->systemService->editSystem($id, $name);

        if (!$success) {
            return $response->withJson(['status' => 'error'], 500);
        }

        return $response->withJson([
            'status' => 'success',
            'systemName' => $name,
            'id' => $id,
        ]);
    }
}
