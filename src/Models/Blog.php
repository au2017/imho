<?php
namespace Imho\Models;

class Blog
{
    public $id;
    public $game;
    public $body;
    public $title;
    public $userId;
    public $dateCreated;

    public function __construct(int $id = null, string $title = null,
        string $body = null, Game $game = null, int $userId = null,
        string $dateCreated = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->body = $body;
        $this->game = $game;
        $this->userId = $userId;
        $this->dateCreated = $dateCreated;
    }
}
