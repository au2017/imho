<?php
namespace Imho\Models;

class Game
{
    public $id;
    public $title;
    public $system;
    public $year;
    public $completed;

    public function __construct(int $id = null, string $title = null,
        System $system = null, int $year = null, bool $completed = false)
    {
        $this->id = $id;
        $this->title = $title;
        $this->system = $system ?? new System();
        $this->year = $year;
        $this->completed = $completed;
    }

    public function completedAsText() {
        return $this->completed ? 'completed' : 'not completed';
    }
}
