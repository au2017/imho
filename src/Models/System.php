<?php
namespace Imho\Models;

class System
{
    public $id;
    public $title;

    public function __construct(int $id = null, string $title = null)
    {
        $this->id = $id;
        $this->title = $title;
    }
}
