<?php
namespace Imho;

use \Memcached;
use \Psr\Container\ContainerInterface;
use \Doctrine\DBAL\Configuration;
use \Doctrine\DBAL\DriverManager;
use \DI\ContainerBuilder;
use \DI\Bridge\Slim\App;
use \Slim\Views\Twig;
use \Slim\Views\TwigExtension;
use \Imho\Repos\Interfaces as RepoInterfaces;
use \Imho\Repos\Implementations as RepoImplementations;
use \Imho\Services\Interfaces as ServiceInterfaces;
use \Imho\Services\Implementations as ServiceImplementations;

final class MyApp extends App
{
    protected function configureContainer(ContainerBuilder $builder)
    {
        $definitions = [
            /**
            * Environment settings
            */
            'settings.displayErrorDetails' => true,

            /**
            * Twig template setup
            */
            Twig::class => function (ContainerInterface $c) {
                $twig = new Twig('templates', [
                    //'cache' => 'cache',
                    'debug' => true,
                ]);

                $twig->addExtension(new TwigExtension(
                    $c->get('router'),
                    $c->get('request')->getUri()
                ));

                return $twig;
            },

            /**
             * session helper
             */
            'session' => function ($c) {
                return new \SlimSession\Helper();
            },

            /**
            * Doctrine DBAL settings and registration
            */
            'dbConnection' => function ($c) {
                $config = new Configuration();
                $params = ['url' => 'mysql://andrew:welcome1@brain.lan/gamedb'];

                return DriverManager::getConnection($params, $config);
            },

            /**
            * Logging config
            */
            'logger' => function ($c) {
                $handler = new \Monolog\Handler\StreamHandler(
                    'logs/imho.log',
                    \Monolog\Logger::DEBUG
                );

                return new \Projek\Slim\Monolog('imho', [
                    'directory' => 'logs',
                    'filename' => 'imho.log',
                    'timezone' => 'America/New_York',
                    'level' => 'debug',
                    'handlers' => [ ],
                ]);
            },

            /**
             * Memcached config
             */
            'cache' => function ($c) {
                $memcached = new \Memcached();
                $memcached->addServer('localhost', 11211);

                return $memcached;
            },

            /**
            * DI interface to implementation mappings
            */
            RepoInterfaces\ISystemRepo::class => \DI\object(RepoImplementations\SystemRepo::class),
            RepoInterfaces\IAccountRepo::class => \DI\object(RepoImplementations\AccountRepo::class),
            RepoInterfaces\IGameRepo::class => \DI\object(RepoImplementations\GameRepo::class),
            RepoInterfaces\IBlogRepo::class => \DI\object(RepoImplementations\BlogRepo::class),
            ServiceInterfaces\ISystemService::class => \DI\object(ServiceImplementations\SystemService::class),
            ServiceInterfaces\IAccountService::class => \DI\object(ServiceImplementations\AccountService::class),
            ServiceInterfaces\IGameService::class => \DI\object(ServiceImplementations\GameService::class),
            ServiceInterfaces\IBlogService::class => \DI\object(ServiceImplementations\BlogService::class),
        ];

        $builder->useAnnotations(true);
        $builder->addDefinitions($definitions);
    }
}
