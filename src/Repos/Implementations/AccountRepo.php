<?php
namespace Imho\Repos\Implementations;

final class AccountRepo implements \Imho\Repos\Interfaces\IAccountRepo
{
    /**
     * @Inject("logger")
     */
    private $logger;

    /**
     * @Inject("dbConnection")
     */
    private $conn;

    public function createNewUser(string $username, string $password) : bool
    {
        try {
            $sql = 'INSERT INTO user(username, password) VALUES (?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $username);
            $stmt->bindValue(2, $password);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function getUserRecord(string $username) : array
    {
        try {
            $q = 'SELECT * FROM user WHERE username = ?';
            $stmt = $this->conn->prepare($q);
            $stmt->bindValue(1, $username);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return $stmt->fetch();
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }
}
