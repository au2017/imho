<?php
namespace Imho\Repos\Interfaces;

interface IBlogRepo {
    public function getBlogs() : array;
    public function addBlog(string $title, string $body, int $gameId, int $userId) : int;
    public function editBlog(int $id, string $title, string $body, int $gameId, int $userId) : bool;
    public function deleteBlog(int $id) : bool;
}
