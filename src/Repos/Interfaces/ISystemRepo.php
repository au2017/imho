<?php
namespace Imho\Repos\Interfaces;

interface ISystemRepo
{
    public function getSystems() : array;
    public function deleteSystem(int $id) : bool;
    public function addSystem(string $name) : int;
    public function editSystem(int $id, string $name) : bool;
}
