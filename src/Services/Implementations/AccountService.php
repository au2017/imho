<?php
namespace Imho\Services\Implementations;

use \Imho\Repos\Interfaces\IAccountRepo;
use \Imho\Models\User;

final class AccountService implements \Imho\Services\Interfaces\IAccountService
{
    /**
     * @Inject("logger")
     */
    private $logger;

    private $accountRepo;

    public function __construct(IAccountRepo $acctRepo)
    {
        $this->accountRepo = $acctRepo;
    }

    public function createNewUser(string $username, string $pass, string $confirm) : bool
    {
        try {
            if (empty($username) || empty($pass) || empty($confirm)) {
                return false;
            }

            if ($pass !== $confirm) {
                return false;
            }

            $hash = password_hash($pass, PASSWORD_DEFAULT);

            return $this->accountRepo->createNewUser($username, $hash);
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }

    public function getUser(string $username) : User
    {
        try {
            return new User($this->accountRepo->getUserRecord($username));
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }
}
