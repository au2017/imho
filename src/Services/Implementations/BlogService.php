<?php
namespace Imho\Services\Implementations;

use \Imho\Services\Interfaces\IBlogService;
use \Imho\Repos\Interfaces\IBlogRepo;
use \Imho\Models\Blog;
use \Imho\Models\System;
use \Imho\Models\Game;

final class BlogService implements IBlogService
{
    /**
     * @Inject("logger")
     */
    private $logger;

    /**
     * @Inject("cache")
     */
    private $cache;

    protected $blogRepo;

    const KEY = 'GET_BLOGS';

    public function __construct(IBlogRepo $blogRepo)
    {
        $this->blogRepo = $blogRepo;
    }

    public function getBlogs() : array
    {
        $mapBlogModel = function ($row) {
            $system = new System($row['system_id'], $row['system_title']);
            $game = new Game($row['game_id'], $row['game_title'], $system,
                $row['release_year'], $row['completed']);

            return new Blog($row['id'], $row['title'], $row['body'], $game, null,
                $row['date_created']);
        };

        $blogs = $this->getBlogsFromCache();

        return array_map($mapBlogModel, $blogs);
    }

    public function getBlog(int $id) : Blog
    {
        $blogs = $this->getBlogs();

        foreach($blogs as $blog) {
            if ($id === $blog->id) {
                return $blog;
            }
        }

        return null;
    }

    public function addBlog(string $title, string $body, int $gameId, int $userId) : int
    {
        try {
            $id = $this->blogRepo->addBlog($title, $body, $gameId, $userId);

            if ($id) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $id;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return 0;
        }
    }

    public function editBlog(int $id, string $title, string $body, int $gameId, int $userId) : bool
    {
        try {
            $success = $this->blogRepo->editBlog($id, $title, $body, $gameId, $userId);

            if ($success) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $success;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }

    public function deleteBlog(int $id) : bool
    {
        try {
            $success = $this->blogRepo->deleteBlog($id);

            if ($success) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $success;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }

    private function getBlogsFromCache() : array
    {
        // check for item in cache
        $blogs = $this->cache->get(self::KEY);

        if(!$blogs) {
            // not in cache, so make db call then put in cache
            $blogs = $this->blogRepo->getBlogs();
            $this->cache->add(self::KEY, $blogs);
        }

        return $blogs;
    }
}
