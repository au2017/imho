<?php
namespace Imho\Services\Interfaces;

use \Imho\Models\Blog;

interface IBlogService
{
    public function getBlogs() : array;
    public function getBlog(int $id) : Blog;
    public function addBlog(string $title, string $body, int $gameId, int $userId) : int;
    public function editBlog(int $id, string $title, string $body, int $gameId, int $userId) : bool;
    public function deleteBlog(int $id) : bool;
}
