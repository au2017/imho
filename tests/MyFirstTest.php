<?php

use PHPUnit\Framework\TestCase;

class MyFirstTest extends TestCase
{
    public function testOneEqualsOne()
    {
        $oneStr = '1';
        $oneInt = 1;

        $this->assertEquals($oneStr, $oneInt);
    }
}
